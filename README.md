# Create models from an OpenAPI json file

Openapi spec: https://spec.openapis.org/oas/v3.0.0.html

Requires: https://packagist.org/packages/spatie/enum

## Examples

```
<?php

require __DIR__ . '/vendor/autoload.php';

$creator = new \TTT\OpenApi\Creator(
    __DIR__ . '/openapi.json',
    'TTT\Project\Model',
    '/my-project/Model/',
);

var_dump($creator->saveAll());

```

```
<?php

require __DIR__ . '/vendor/autoload.php';

$creator = new \TTT\OpenApi\Creator(
    __DIR__ . '/openapi.json',
    'TTT\Project\Model',
    '/my-project/Model/',
);

$filter = [
    'Schema1',
    'Schema2',
];

var_dump($creator->saveSchemas($filter));

var_dump($creator->saveHelpers());

```
